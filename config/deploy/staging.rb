role :app, %w(deployer@dnfteam.com)
role :web, %w(deployer@dnfteam.com)
role :db,  %w(deployer@dnfteam.com)

set :rbenv_custom_path, '~/.rbenv'

server 'dnfteam.com', user: 'deployer', roles: %w(web app)