require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(:default, Rails.env)

module DnfTeam
  # Application config class
  class Application < Rails::Application
    # I18n.enforce_available_locales = true
    config.time_zone = 'Europe/Kiev'
    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'rails_admin', '*.{rb,yml}').to_s]
    config.i18n.available_locales = [:ru, :en]
    config.i18n.default_locale = :ru
    # добавлен fallback
    config.i18n.fallbacks = true
    config.i18n.fallbacks = [:en]

    config.email = 'info@dnfteam.com'

    config.autoload_paths += %W(#{config.root}/lib)

    config.assets.initialize_on_precompile = true
    config.assets.precompile += [/.*ru.js/, /.*html5.js/, /.*intro-bold-caps.*/]
  end
end
