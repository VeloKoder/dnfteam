# Time class
class Time
  # Formatted time
  def formatted
    "#{day} #{I18n.localize self, format: '%B'} #{year} #{strftime('%H:%M')}"
  end
end
