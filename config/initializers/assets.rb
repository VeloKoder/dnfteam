Rails.application.assets.register_engine('.slim', Slim::Template)

# Sass module override
module Sass
  # Tree module
  module Tree
    # Import class
    class ImportNode < RootNode
      def _dump(_)
        Marshal.dump([@imported_filename, children])
      end

      def self._load(data)
        filename, children = Marshal.load(data)
        node = ImportNode.new(filename)
        node.children = children
        node
      end
    end
  end
end
