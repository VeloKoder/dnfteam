RailsAdmin.config.model Email do
  object_label_method :value

  exclude_fields :id, :created_at, :updated_at
end
