RailsAdmin.config.model User do

  object_label_method :label_name

  list do
    fields :nickname, :email, :groups, :ranks
    field :birthday do
      formatted_value do
        bindings[:object].birthday.try(:strftime, '%d.%m.%Y')
      end
    end
    field :avatar do
      formatted_value do
        image_tag bindings[:object].avatar.url(:thumb)
      end
    end
    field :location
    field :name do
      formatted_value do
        "#{bindings[:object].surname} #{bindings[:object].name} #{bindings[:object].patronymic}"
      end
    end
    fields :role, :hardware, :guild, :subscription, :created_at, :updated_at
  end

  edit do
    fields :email, :password, :nickname, :name, :patronymic, :surname, :birthday, :hardware, :location,
           :subscription, :avatar, :guild, :ranks, :awards, :groups
  end

  exclude_fields :id, :reset_password_sent_at, :remember_created_at, :slug, :current_sign_in_ip, :current_sign_in_at
end
