RailsAdmin.config.model Article do
  configure :content, :ck_editor

  edit do
    fields :name, :description

    field :user_id, :hidden do
      formatted_value do
        bindings[:view]._current_user.id
      end
    end

    fields :groups, :content
  end
end
