DnfTeam::Application.configure do
  config.cache_classes = true

  config.eager_load = true

  config.consider_all_requests_local       = false

  config.action_controller.perform_caching = true

  config.serve_static_assets = false
  config.assets.js_compressor = :uglifier
  config.assets.css_compressor = :sass
  config.assets.compile = false
  config.assets.digest = true
  config.assets.version = '1.0'
  config.assets.paths << "#{Rails.root}/app/assets/classic"
  config.assets.paths << "#{Rails.root}/app/assets/fonts"

  config.i18n.fallbacks = true

  config.active_support.deprecation = :notify

  config.action_mailer.delivery_method = :sendmail
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_options = {from: 'info@dnfteam.com'}

  NonStupidDigestAssets.whitelist = [
    'ckeditor/config.js',
    'ckeditor/ckeditor.js',
    'ckeditor/styles.js',
    'ckeditor/contents.css',
    %r{ckeditor\/skins\/moono\/.*}i,
    'ckeditor/lang/ru.js',
    'ckeditor/plugins/dialog/dialogDefinition.js',
    %r{ckeditor\/plugins\/(link|magicline|flash|table)\/.*}i,
    /classic\/.*/
  ]

  config.log_formatter = ::Logger::Formatter.new
  config.log_level = :info
end
