DnfTeam::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  devise_for :users

  root to: 'dashboard#index'

  scope path: 'remote' do
    resources :popups, only: :show
    resource :order, only: :create
  end

  devise_scope :user do
    get 'users/:id' => 'users#show', as: :user
    put '/users/:id/update' => 'users#update', as: :update_user_profile
  end

  get ':id' => 'static_pages#show', as: :static_page

  resources :lessons, :users, only: :show
end
