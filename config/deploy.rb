# config valid only for Capistrano 3.2.1
lock '3.2.1'

set :application, 'dnfteam'
set :repo_url, 'git@bitbucket.org:VeloKoder/dnfteam.git'

set :deploy_to, '/home/deployer/dnfteam_production'

# Default value for :pty is false
set :pty, true

set :linked_files, %w(config/database.yml .rbenv-vars)
set :linked_dirs, %w(bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/ckeditor_assets)
# set :rbenv_ruby, '2.1.2'
# set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all
# set :rbenv_custom_path, '/home/deployer/.rbenv'
set :default_env, ({ path: '/opt/rbenv/shims:$PATH' })

set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart
end

desc 'Invoke a rake command on the remote server'
task :invoke, [:command] => 'deploy:set_rails_env' do |_, args|
  on primary(:app) do
    within current_path do
      with rails_env: fetch(:rails_env) do
        rake args[:command]
      end
    end
  end
end
