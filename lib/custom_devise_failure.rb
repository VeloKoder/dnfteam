# Devise methods override
class CustomDeviseFailure < Devise::FailureApp
  def redirect_url
    root_path
  end

  def respond
    if http_auth?
      http_auth
    else
      flash[:notice] = 'Неверный логин или пароль!'
      redirect
    end
  end
end
