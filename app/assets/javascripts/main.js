﻿var heightAll,
    $name;

function initializeMask() {
    if ($('#user_birthday').length)
    {
        $('#user_birthday').mask('99.99.9999');
    }

    if ($('#order_birthday').length)
    {
        $('#order_birthday').mask('99.99.9999');
    }
};

function positionPopup() {
  var height = $('#'+$name+'').height(),
      winheight= $(window).height(),
      marg = Math.round((winheight-height)/2);
  if ($name == 'start')
  {
      var top = 150;
  }
  else
  {
      var top = $(window).scrollTop();
  }

  if ((height > winheight) || $name == 'registration') {
    $('#' + $name).css({
      'position':'absolute',
      'top': top
    })
  }
  else {
    $('#' + $name).css({
      'position':'fixed',
      'top':marg
    })
  }
  if ($name = 'noteInner') {
    $('#'+$name+'').css({'position':'relative'});
  }
}
//open notification
$('div.popup form').submit(function() {
  $name='noteInner';
  $('#hideNotification, #notification').fadeIn();
  positionPopup();
});
//close notification
$(document).on('click', '#hideNotification', function() {
  $('#notification').animate({'left': '100%'},function() {$(this).fadeOut(300);});
});

function HeightImportant() {
  var el = $('div.important').find('div.content-center'),
      height = $('div.important').find('div.caption').outerHeight(),
      padding = parseInt(el.css('padding-top') +  el.css('padding-top')),
      heightAll = height + padding;
  $('div.important').css('height', heightAll); 
}

//checkbox
function setupLabelCheck() {
  if ($('.label_check input').length) {
    $('.label_check').each(function(){ 
        $(this).removeClass('c_on');
    });
    $('.label_check input:checked').each(function(){ 
        $(this).parent('label').addClass('c_on');
    });                
  };
};

function setupLabelRadio() {
  if ($('.label_radio input').length) {
    $('.label_radio').each(function(){ 
        $(this).removeClass('r_on');
    });
    $('.label_radio input:checked').each(function(){ 
        $(this).parent('label').addClass('r_on');
    });
  };
}


//position TS viewer
function positionViewer() {
  var k = 672,
      height = $('#popupTsViewer').outerHeight(),
      winheight= $(window).height(),
      marg = Math.round((winheight-height)/2),
      heightLink = parseInt($('#linkTsViewer').outerHeight()),
      margTop;
  if (k >= winheight) {
    height = winheight-10;
    $('#popupTsViewer').css({
      'height':winheight-10,
      'top':3
    });
  }
  else {
    height = k;
    $('#popupTsViewer').css({
      'height':k,
      'top':marg
    });
  }
  setTimeout(function() {
    margTop = Math.round((height - heightLink)/2)
    $('#linkTsViewer').css({'margin-top':margTop});
  },50);
}

$(document).on('ready page:change', function(){
  $('body').addClass('js');
  if($('div.important').length) {
    HeightImportant(); 
  }
  
  $(".rslides").responsiveSlides({
    namespace: "transparent-btns",
    nav: true,
    timeout: slider_speed,
    random: true
  });
  $(window).load(function() {
    if($('div.important').length) {
      setTimeout(function() {
        $('div.important').animate({'opacity':1},300);
      },500);
      var heightS = $('div.slider').height(),
          el = $('div.important').find('div.content-center'),
          height = $('div.important').find('div.caption').outerHeight(),
          padding = parseInt(el.css('padding-top') +  el.css('padding-top')),
          heightAll = height + padding;
      $('div.important').on({
        mouseenter: function() {
          $( this ).stop().addClass('important-open').animate({'height': heightS},300);
        },
        mouseleave: function() {
          $( this ).stop().removeClass('important-open').animate({'height': heightAll},300);
        }
      });
    }
  });
  $(window).scroll(function() {
    var top = $(document).scrollTop();
     if (top > 105) {
      $('header').addClass('fix');
     }
     else {
      $('header').removeClass('fix');
    }
  });
  $(window).resize(function() {
    if ($('div.popup').length) {
      positionPopup();
    }
    positionViewer();
  });
  
  //close popups
  $(document).on('click', '.close', function() {
    if ($(this).is('.close-popup'))
    {
      var element = $(this);
      element.closest('div.popup').fadeOut(200);
      $('#hideContent').fadeOut();
      if (element.is('.flash-notice')) {
          $('#flash-notice').css('display', 'none');
      }
      else {
         element.closest('.popup-inner').remove();
      }

    }
    else {
      $(this).closest('div.important').fadeOut(200);
    }
  });
  $('#hideContent').click(function() {
      $('div.popup').fadeOut(200);
      $(this).fadeOut();
  });
  $(document).on('click', '#additionalFields', function() {
    $('#moreField').slideDown(200);
    setTimeout(function() {
      positionPopup();
    },100);
    $(this).attr('id', 'additionalFieldsHide');
    return false;
  });

  $(document).on('click', '#additionalFieldsHide', function() {
    $('#moreField').css('display', 'none');
    $(this).attr('id', 'additionalFields');
    return false;
  });

  
  positionViewer();
  //open TS viewer
  var widthLink = $('#linkTsViewer').outerWidth();
  $('#popupTsViewer').on({
    mouseenter: function() {
      var el = $('#popupTsViewer'),
          widthTs = widthLink + $('#tsViewer').outerWidth();
      el.addClass('open');
      el.stop().animate({'margin-left':-widthTs},300);
    },
    mouseleave: function() {
      var el = $('#popupTsViewer');
      el.removeClass('open');
      el.stop().animate({'margin-left':-widthLink},300);
    }
  });
  //checkbox initial
  $(document).on('click', '.label_check', function(){
    setupLabelCheck();
  });
  $(document).on('click', '.label_radio', function(){
    setupLabelRadio();
  })
  //colorbox
  $(document).on('ready page:change', function(){
    if ($('.group1').length) {
      $(".group1").colorbox({
        transition:"fade",
        maxWidth:'90%'
      });
    };
      $(window).resize(function(){
      if ($('.group1').length) {
        $.colorbox.resize();
      }
    });
  });
  //placeholder
	jQuery(function(){
    if ($('input[placeholder]').length || $('textarea[placeholder]').length ) {
      jQuery('input[placeholder], textarea[placeholder]').placeholder();
    }
	});
});


//open popups
$(document).on('click', '.linkPopup', function() {
    var link = $(this),
        url = link.attr('data-url');

    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $name = link.attr('data-href');
            $('#' + $name).html(data);
            $('div.popup').hide();
            $('#' + $name).fadeIn(200);
            positionPopup();
            if ($('#hideContent').is(':hidden'))
            {
                $('#hideContent').fadeIn();
            }
            initializeMask();
            return false;
        }
    });
    return false;
});

$(document).on('click', '#showMore', function(){
  $.ajax({
    url: $(this).attr('data-url'),
    dataType: 'html',
    success: function(data){
      $('#ajax-append').find('.show-more').remove();
      $('#ajax-append').append($(data));
    }});
  return false;
});

$(document).on('ready page:change', function(){
  initializeMask();
});