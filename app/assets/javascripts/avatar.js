function readURL(input) 
{
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('.avatar-block img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$(document).on('ready page: change', function()
{
  $('#deleteAvatar').click(function()
  {
    $('#user_delete_image').val(1);
    $('.avatar-block img').attr('src', '');
    $(this).css('display', 'none');

    if ($('#remote_check').val())
    {
      $('#edit_user').submit();
    }
  });

  $('#user_avatar').change(function(){
    readURL(this);
    $('#deleteAvatar').css('display', 'block');

    if ($('#remote_check').val())
    {
      $('#edit_user').submit();
    }
  });
})