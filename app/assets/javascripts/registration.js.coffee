$(document).on 'change', '#rules', ->
  if document.getElementById('rules').checked
    $('#registration_submit').show()
  else
    $('#registration_submit').hide()

$(document).on 'change', '.guild', ->
  $.ajax
    url: '/remote/popups/order'
    data: { guild: $(this).val() }
    type: 'GET'
    dataType: 'html'
    success: (data) ->
      $('#start').html data

$(document).on 'keyup', '.order_field', ->
  form = $(this.form)
#  form.validate()
  show_checking = false
  $('.label_radio').each (i, e) ->
    show_checking = true if $(e).attr('class').indexOf('r_on') != -1
  if show_checking
    form.find('.order_field').each (i, e) ->
      if $(e).val().length < 1
        show_checking = false
  $('.rules_check').show() if show_checking

$(document).on 'click', '#registration_submit', ->
  form = $(this.form)
  form.validate()
  unless form.valid()
    form.find('.order_field').each (i, e) ->
      color = if $(e).valid() then 'green' else 'red'
      $(e).css('border', "1px solid #{color}")
    false
