# Mailer for order
class OrderMailer < ActionMailer::Base
  default from: Rails.application.config.email
  layout 'application_mailer'

  def client(order)
    @order = order
    mail(to: @order.email, subject: 'DNFTEAM: Вы отправили заявку!').deliver
  end

  def guild(order)
    @order = order
    mail(to: @order.guild.emails.map(&:value).join(','), subject: 'DNFTEAM: Вам поступила новая заявка!').deliver
  end
end
