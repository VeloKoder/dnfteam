# Static pages class
class StaticPage < ActiveRecord::Base
  extend FriendlyId

  has_and_belongs_to_many :groups

  friendly_id :name, use: [:slugged, :finders]

  validates :content, :name, presence: true

  include Granteable
end
