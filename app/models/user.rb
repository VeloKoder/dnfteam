# User model
class User < ActiveRecord::Base
  extend FriendlyId

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :delete_image

  friendly_id :nickname, use: [:slugged, :finders]

  belongs_to :guild

  has_many :lessons
  has_many :advertisement
  has_many :articles
  has_many :rank_histories
  has_many :award_histories

  has_many :ranks,  through: :rank_histories
  has_many :awards, through: :award_histories

  has_and_belongs_to_many :groups

  has_attached_file :avatar,
                    styles: {
                      thumb: '100x100>',
                      regular: '200x200>'
                    },
                    default_url: 'missing.png'

  validates :email, :nickname, presence: true
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  include Granteable

  before_save :set_uniqueness_nickname, if: [:nickname, :nickname_changed?]
  before_save :destroy_image?
  before_save :set_role

  # get user's fullname
  def fullname
    [name, patronymic, surname].delete_if(&:blank?).join(' ')
  end

  # get user's labelname
  def label_name
    nickname.presence || fullname.presence || 'Аноним'
  end

  # user's name setter
  def fullname=(str)
    str = str.split(' ')
    self.name = str.shift
    self.patronymic = str.shift
    self.surname = str.shift
  end

  private

  # setting role from group
  def set_role
    self.role = groups.map(&:role).sort_by { |x| %w(root admin user).index(x) }.first
  end

  # checking for uniqueness of nickname
  def set_uniqueness_nickname
    nicknames = User.select(:nickname).map(&:nickname)
    return unless nickname.in? nicknames
    i = 0
    while nickname.in? nicknames
      self.nickname += i.to_s
      i += 1
    end
  end

  # checking for destroying image
  def destroy_image?
    avatar.clear if @delete_image == '1'
  end
end
