# Dashboard images class
class DashboardImage < ActiveRecord::Base

  has_attached_file :attach,
                    styles: { content: '1920х600' },
                    url:  '/system/dashboard_images/:id/:filename',
                    path: ':rails_root/public/system/dashboard_images/:id/:filename'

  validates_attachment_presence :attach
  validates_attachment_size :attach, less_than: 2.megabytes
  validates_attachment_content_type :attach, content_type: /\Aimage/

  after_save :set_interval

  private

  # Setting slider speed
  def set_interval
    self.class.skip_callback :save, :after, :set_interval
    images = DashboardImage.where.not(id: id)
    if interval.blank?
      self.interval = images.first.interval
      save
    else
      images.each do |image|
        image.update(interval: interval)
      end
    end
    self.class.set_callback :save, :after, :set_interval
  end
end
