# Users group class
class Group < ActiveRecord::Base
  extend Enumerize

  enumerize :role, in: %w(root admin user)

  has_and_belongs_to_many :users
  has_and_belongs_to_many :static_pages
  has_and_belongs_to_many :lessons
  has_and_belongs_to_many :guilds
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :articles
  has_and_belongs_to_many :lesson_categories

  scope :defaults, -> { where(is_default: true) }

  after_save :update_users_role, if: :role_changed?

  private

  # set user role from group
  def update_users_role
    users.each { |u| u.save validate: false }
  end
end
