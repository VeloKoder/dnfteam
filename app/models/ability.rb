# Abilities class
class Ability
  include CanCan::Ability

  # initializer
  def initialize(user)
    return unless user
    if user.role != 'user'
      can :access, :rails_admin
      can :dashboard
      can :manage, :all
    else
      can :manage, :user, role: 'user'
    end
  end
end
