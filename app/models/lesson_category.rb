# Lessons categories class
class LessonCategory < ActiveRecord::Base
  belongs_to :lesson

  has_many :lessons

  has_and_belongs_to_many :groups

  validates :name, presence: true

  include Granteable
end
