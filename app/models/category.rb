# Category class
class Category < ActiveRecord::Base
  extend FriendlyId

  friendly_id :name, use: [:slugged, :finders]

  has_many :lessons

  has_and_belongs_to_many :groups

  validates :name, presence: true

  include Granteable
end
