# Guild class
class Guild < ActiveRecord::Base
  has_attached_file :image, styles: { small: 'x81' }

  has_many :emails
  has_many :users

  has_and_belongs_to_many :groups

  validates_attachment_presence :image
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates :name, :link, :structure, :orders, :charter, presence: true
  validates :emails, presence: true, if: :in_order

  scope :for_order, -> { where(in_order: true) }

  include Granteable
end
