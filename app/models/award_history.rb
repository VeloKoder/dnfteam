# Awards history model
class AwardHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :award
end
