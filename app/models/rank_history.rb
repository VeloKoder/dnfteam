# Rank history class
class RankHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :rank
end
