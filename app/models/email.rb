# Email class
class Email < ActiveRecord::Base
  belongs_to :guild

  validates :value, presence: true
end
