# Previleges module
module Granteable
  extend ActiveSupport::Concern

  # class methods
  module ClassMethods
    # get previleges
    def granted_for_groups(groups)
      ids = groups.map { |group| group.send(name.downcase.pluralize).pluck(:id) }.flatten
      ids.present? ? where(id: ids) : limit(0)
    end
  end
end
