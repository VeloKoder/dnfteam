# Lessons class
class Lesson < ActiveRecord::Base
  extend FriendlyId

  friendly_id :name, use: [:slugged, :finders]

  belongs_to :category
  belongs_to :user
  belongs_to :lesson_category

  has_many :lesson_categories

  has_and_belongs_to_many :groups

  validates :name, :user, presence: true

  include Granteable
end
