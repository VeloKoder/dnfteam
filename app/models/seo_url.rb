# Модель метатегов по ссылкам
class SeoUrl < ActiveRecord::Base
  has_many :urls

  validates :urls, presence: true
end
