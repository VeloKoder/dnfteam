# Ranks class
class Rank < ActiveRecord::Base
  has_many :rank_histories
  has_many :users, through: :rank_histories
end
