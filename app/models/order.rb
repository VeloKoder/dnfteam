# Order class
class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :guild

  validates :guild_id, presence: true
end
