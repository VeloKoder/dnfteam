# Links module
class Link < ActiveRecord::Base
  extend Enumerize

  enumerize :mode, in: %w(email channel vk)

  validates :mode, :title, :url, presence: true

  scope :find_by_mode, -> (mode) { where(mode: mode) }
end
