# Url model
class Url < ActiveRecord::Base
  belongs_to :seo_url

  validates :value, presence: true
end
