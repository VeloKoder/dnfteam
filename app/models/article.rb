# Article class
class Article < ActiveRecord::Base
  belongs_to :user

  has_and_belongs_to_many :groups

  validates :name, presence: true

  before_save :default_groups

  private

  # set default groups
  def default_groups
    self.groups = Group.all if groups.blank?
  end

  include Granteable
end
