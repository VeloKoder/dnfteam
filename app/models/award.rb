# Award history
class Award < ActiveRecord::Base
  has_many :award_histories
  has_many :users, through: :award_histories
end
