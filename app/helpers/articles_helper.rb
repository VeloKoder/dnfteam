# Methods for articles
module ArticlesHelper
  # slider inserting
  def with_slider(object, field = :content, group = nil)
    group ||= object.id
    content = object.send(field).gsub(
      /(<img.*src="(.*)content_(.+)".*\/>)/,
      '<a class="group1 cboxElement" href="\2original_\3" rel="group' + group.to_s + '">\1</a>'
    )
    ('<div class="img-slide">' + content + '</div>').html_safe
  end

  # Checking for final page
  def final_page?(model, start_per = 3, per = 5)
    page = params[:page].try(:to_i) || 1
    total_count = model.count
    return true if total_count <= start_per
    total_count -= start_per
    last_page = total_count / per + 1
    last_page += 1 if total_count % 5 > 0
    page >= last_page
  end
end
