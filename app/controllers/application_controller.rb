# Application controller
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied, with: :access_denied_error
  rescue_from CanCan::AccessDenied, with: :access_denied_error

  before_action :configure_devise_permitted_parameters, if: :devise_controller?
  before_action :initialize_groups, :menu, :check_access
  before_action :initialize_static_pages, unless: :user_signed_in?
  before_action :initialize_slider

  private

  # categories and links initializer
  def menu
    @links = Link.all
    @guilds = Guild.where(id: @groups.map { |group| group.guilds.pluck(:id) }.flatten.uniq)
    @categories = @groups.map(&:categories).flatten.uniq
  end

  # user groups initializer
  def initialize_groups
    @groups = current_user.groups if user_signed_in?
    @groups = Group.defaults if @groups.blank?
  end

  # access control
  def check_access
    return if is_a?(DashboardController) || action_name.eql?('dashboard')
    return if is_a?(UsersController) && params[:id].eql?(current_user.try(:nickname))
    if is_a? RailsAdmin::MainController
      field = "is_admin_#{params[:action].eql?('edit') ? 'edit' : 'view'}_#{params[:model_name]}"
      return unless field.in? Group.column_names
      return if @groups.where(field => true).exists?
    else
      rel = self.class.name.downcase.gsub('controller', '')

      return unless rel.to_sym.in? Group.reflections.keys
      return unless "is_#{rel}".in? Group.column_names
      return if @groups.where("is_#{rel}").exists?

      ids = @groups.map { |group| group.send(rel).pluck(:id) }.flatten.delete_if(&:blank?).uniq
      return if params[:action].eql?('show') && params[:id].in?(ids)
    end

    fail CanCan::AccessDenied
  end

  # Get static info for all pages
  def initialize_static_pages
    @static_pages = StaticPage.all
  end

  # Set metatags
  def metatags(object = nil)
    template = Url.where(value: request.original_fullpath)
    if template.present?
      template = template.first.seo_url
      title = template.title
      description = template.description
    elsif object
      text_part = object.is_a?(User) ? "#{object.fullname} - просмотр профиля" : object.name
      title = "#{text_part} | DNFTEAM"
      description = "#{text_part} на DNFTEAM"
    end
    set_meta_tags title: title, description: description
  end

  # Return to current page after registration
  def after_sign_in_path_for(resource)
    stored_location_for(resource) || request.referer || root_path
  end

  def initialize_slider
    @slider = DashboardImage.all
  end

  # 403 page rendering
  def access_denied_error
    render 'layouts/403', status: 403, layout: 'application'
  end

  protected

  def configure_devise_permitted_parameters
    registration_params = [
      :nickname, :email, :password, :password_confirmation, :name, :surname, :patronymic
    ]

    if params[:action] == 'update'
      devise_parameter_sanitizer.for(:account_update) do |u|
        u.permit(registration_params << :current_password)
      end
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) do |u|
        u.permit(registration_params) 
      end
    end
  end
end
