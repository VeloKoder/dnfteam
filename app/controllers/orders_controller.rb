# Orders controller
class OrdersController < ApplicationController
  # new order
  def create
    order = Order.new(order_params)
    order.email ||= current_user.email if user_signed_in?
    order.nickname ||= current_user.nickname
    @result = order.save
    @result = OrderMailer.guild(order) if @result
    @result = OrderMailer.client(order) if @result && order.email.present?
    @result =
      if @result
        'Заявка отправлена, с Вами свяжутся'
      else
        'Не удалось отправить заявку. Проверьте правильность данных или повторите позже'
      end

    respond_to do |format|
      format.js
    end
  end

  private

  # order parameters
  def order_params
    params.require(:order).permit(
      :id, :email, :name, :hardware, :location, :nickname, :experience, :contacts,
      :languages, :available_days, :dialog_time, :birthday, :guild_id, :user_id
    )
  end
end
