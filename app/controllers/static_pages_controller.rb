# Controller for pages with static info
class StaticPagesController < ApplicationController
  # Static page shoving
  def show
    @page = StaticPage.find(params[:id])
    fail(CanCan::AccessDenied) unless @page.id.in? @groups.map(&:static_page_ids).flatten.uniq
    metatags(@page)
  end

  # Markup showing
  def sp
    render params[:page]
  end
end
