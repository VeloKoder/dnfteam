# Users controller
class UsersController < Devise::RegistrationsController
  include Devise::Controllers::Helpers

  before_action :set_user

  # user profile viewing
  def show
    @user = User.find(params[:id])
    metatags(@user)
  end

  # user editing
  def edit
  end

  # изменение
  def update
    result = @user.update(user_params)
    if user_params[:password] || user_params[:email]
      flash[:notice] = 'Профиль обновлен. Пожалуйста, войдите на сайт еще раз.'
      sign_out_and_redirect(@user)
    else
      flash[:notice] = 'Профиль обновлен.'
      redirect_to(result ? user_path(@user) : :back)
    end
  end

  # getting current user
  def set_user
    @user = current_user
  end

  private

  # параметры
  def user_params
    params.require(:user).permit(
      :id, :email, :password, :encrypted_password, :password_confirmation, :avatar, :name, :patronymic,
      :surname, :hardware, :location, :birthday, :subscription, :avatar_content_type, :avatar_file_size,
      :avatar_file_name, :country, :reset_password_token, :reset_password_sent_at, :nickname, :experience,
      :contacts, :languages, :available_days, :dialog_time, :delete_image, :fullname
    )
  end
end
