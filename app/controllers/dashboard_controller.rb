# Main page controller
class DashboardController < ApplicationController
  # articles list
  def index
    page = params[:page] ? params[:page].to_i : 1
    start_index = 0
    finish_index = 2
    if page > 1
      start_index += 3 + 5 * (page - 2)
      finish_index = start_index + 5
    end
    @articles = Article.granted_for_groups(@groups).order(created_at: :desc).to_a[start_index..finish_index]
    if request.xhr?
      render partial: 'articles_with_paginate', layout: false
    else
      @advertisement = Advertisement.first
      metatags
    end
  end
end
