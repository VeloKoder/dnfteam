# Lessons controller
class LessonsController < ApplicationController
  # Show lesson content
  def show
    @lesson = Lesson.find(params[:id])

    metatags(@lesson)
  end
end
