# Popups for ordering, entering and registration
class PopupsController < ApplicationController
  # show popup
  def show
    if params[:id].eql? 'registration'
      @user = User.new
    elsif params[:id].in? %w(email password)
      @user = current_user
    elsif params[:id].eql? 'order'
      @order = Order.new(user_id: current_user.try(:id))
      @guild = Guild.find(params[:guild]) if params[:guild]
    end
    render params[:id], layout: false
  end
end
