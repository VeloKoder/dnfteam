# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150227125746) do

  create_table "advertisements", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "articles", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "articles_groups", force: true do |t|
    t.integer "group_id"
    t.integer "article_id"
  end

  create_table "award_histories", force: true do |t|
    t.integer  "award_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "awards", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true, using: :btree

  create_table "categories_groups", force: true do |t|
    t.integer "group_id"
    t.integer "category_id"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "dashboard_images", force: true do |t|
    t.string   "attach_file_name"
    t.string   "attach_content_type"
    t.integer  "attach_file_size"
    t.datetime "attach_updated_at"
    t.integer  "interval"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emails", force: true do |t|
    t.string   "value"
    t.integer  "guild_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "groups", force: true do |t|
    t.string   "name"
    t.boolean  "is_admin"
    t.boolean  "is_articles"
    t.boolean  "is_orders"
    t.boolean  "is_lessons"
    t.boolean  "is_categories"
    t.boolean  "is_users"
    t.boolean  "is_admin_view_user"
    t.boolean  "is_admin_view_lesson"
    t.boolean  "is_admin_view_lesson_category"
    t.boolean  "is_admin_view_category"
    t.boolean  "is_admin_view_link"
    t.boolean  "is_admin_view_static_page"
    t.boolean  "is_admin_view_url"
    t.boolean  "is_admin_edit_user"
    t.boolean  "is_admin_edit_lesson"
    t.boolean  "is_admin_edit_lesson_category"
    t.boolean  "is_admin_edit_category"
    t.boolean  "is_admin_edit_link"
    t.boolean  "is_admin_edit_static_page"
    t.boolean  "is_admin_edit_url"
    t.boolean  "is_default"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role",                          default: "user"
    t.boolean  "is_order",                      default: true
  end

  create_table "groups_guilds", force: true do |t|
    t.integer "group_id"
    t.integer "guild_id"
  end

  create_table "groups_lesson_categories", force: true do |t|
    t.integer "group_id"
    t.integer "lesson_category_id"
  end

  create_table "groups_lessons", force: true do |t|
    t.integer "group_id"
    t.integer "lesson_id"
  end

  create_table "groups_static_pages", force: true do |t|
    t.integer "group_id"
    t.integer "static_page_id"
  end

  create_table "groups_users", force: true do |t|
    t.integer "group_id"
    t.integer "user_id"
  end

  create_table "guilds", force: true do |t|
    t.string   "name"
    t.string   "link"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "register_fields"
    t.string   "charter"
    t.string   "structure"
    t.string   "orders"
    t.boolean  "is_email",                 default: true
    t.boolean  "is_password",              default: true
    t.boolean  "is_password_confirmation", default: true
    t.boolean  "is_name",                  default: true
    t.boolean  "is_hardware",              default: true
    t.boolean  "is_birthday",              default: true
    t.boolean  "is_location",              default: true
    t.boolean  "is_nickname",              default: true
    t.boolean  "is_experience",            default: true
    t.boolean  "is_contacts",              default: true
    t.boolean  "is_languages",             default: true
    t.boolean  "is_available_days",        default: true
    t.boolean  "is_dialog_time",           default: true
    t.boolean  "in_order",                 default: true
  end

  create_table "lesson_categories", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "lesson_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lessons", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "content"
    t.integer  "category_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "lesson_category_id"
  end

  add_index "lessons", ["slug"], name: "index_lessons_on_slug", unique: true, using: :btree

  create_table "links", force: true do |t|
    t.string   "url"
    t.string   "mode"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.string  "email"
    t.string  "name"
    t.string  "hardware"
    t.string  "location"
    t.string  "nickname"
    t.string  "experience"
    t.string  "contacts"
    t.string  "languages"
    t.string  "available_days"
    t.string  "dialog_time"
    t.date    "birthday"
    t.integer "guild_id"
    t.integer "user_id"
  end

  create_table "rank_histories", force: true do |t|
    t.integer  "user_id"
    t.integer  "rank_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ranks", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seo_urls", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "static_pages", force: true do |t|
    t.string   "name"
    t.text     "content",    limit: 2147483647
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  add_index "static_pages", ["slug"], name: "index_static_pages_on_slug", unique: true, using: :btree

  create_table "urls", force: true do |t|
    t.string   "value"
    t.integer  "seo_url_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "patronymic"
    t.string   "surname"
    t.date     "birthday"
    t.text     "subscription"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "nickname"
    t.string   "slug"
    t.string   "role",                   default: "user"
    t.string   "location"
    t.string   "hardware"
    t.integer  "guild_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

end
