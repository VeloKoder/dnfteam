class AddRoleToGroup < ActiveRecord::Migration
  def up
    add_column :groups, :role, :string, default: 'user'
  end

  def down
    remove_column :groups, :role
  end
end
