class RemoveFieldsFromUser < ActiveRecord::Migration
  def up
		remove_column :users, :hardware
		remove_column :users, :location
		remove_column :users, :experience
		remove_column :users, :contacts
		remove_column :users, :languages
		remove_column :users, :available_days
		remove_column :users, :dialog_time
  end

  def down
  	add_column :users, :hardware, 			:string
		add_column :users, :location, 			:string
		add_column :users, :experience, 		:string
		add_column :users, :contacts, 			:string
		add_column :users, :languages, 			:string
		add_column :users, :available_days, :string
		add_column :users, :dialog_time, 		:string
  end
end
