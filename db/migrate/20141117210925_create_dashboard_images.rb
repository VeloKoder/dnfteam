class CreateDashboardImages < ActiveRecord::Migration
  def change
    create_table :dashboard_images do |t|
      t.attachment :attach
      t.integer :interval

      t.timestamps
    end
  end
end
