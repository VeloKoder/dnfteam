class AddFieldsToGuild < ActiveRecord::Migration
  def up
    add_column :guilds, :is_email,                 :boolean, default: true
    add_column :guilds, :is_password,              :boolean, default: true
    add_column :guilds, :is_password_confirmation, :boolean, default: true
    add_column :guilds, :is_name,                  :boolean, default: true
    add_column :guilds, :is_hardware,              :boolean, default: true
    add_column :guilds, :is_birthday,              :boolean, default: true
    add_column :guilds, :is_location,              :boolean, default: true
    add_column :guilds, :is_nickname,              :boolean, default: true
    add_column :guilds, :is_experience,            :boolean, default: true
    add_column :guilds, :is_contacts,              :boolean, default: true
    add_column :guilds, :is_languages,             :boolean, default: true
    add_column :guilds, :is_available_days,        :boolean, default: true
    add_column :guilds, :is_dialog_time,           :boolean, default: true
  end

  def down
    remove_column :guilds, :is_email              
    remove_column :guilds, :is_password
    remove_column :guilds, :is_password_confirmation
    remove_column :guilds, :is_name
    remove_column :guilds, :is_hardware
    remove_column :guilds, :is_birthday
    remove_column :guilds, :is_location
    remove_column :guilds, :is_nickname
    remove_column :guilds, :is_experience
    remove_column :guilds, :is_contacts
    remove_column :guilds, :is_languages
    remove_column :guilds, :is_available_days
    remove_column :guilds, :is_dialog_time
  end
end
