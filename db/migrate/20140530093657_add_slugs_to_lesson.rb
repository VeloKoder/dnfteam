class AddSlugsToLesson < ActiveRecord::Migration
  def up
    add_column :lessons, :slug, :string
    add_index :lessons, :slug, unique: true
  end

  def down
    remove_index :lessons, :slug
    remove_column :lessons, :slug
  end
end
