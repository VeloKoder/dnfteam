class AddAvailableDaysToUser < ActiveRecord::Migration
  def change
    add_column :users, :available_days, :string
  end
end
