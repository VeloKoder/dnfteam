class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.string     :name
      t.text       :description
      t.text 	     :content
      t.belongs_to :category
      t.belongs_to :user
      t.timestamps
    end
  end
end
