class CreateOrder < ActiveRecord::Migration
  def up
    create_table :orders do |t|
    	t.string     :email
			t.string     :name
			t.string     :hardware
			t.string     :location
			t.string     :nickname
			t.string     :experience
			t.string     :contacts
			t.string     :languages
			t.string     :available_days
			t.string     :dialog_time
			t.date       :birthday
			t.belongs_to :guild
			t.belongs_to :user
    end
  end

  def down
  	drop_table :orders
  end
end
