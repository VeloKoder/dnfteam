class CreateLessonCategories < ActiveRecord::Migration
  def up
    create_table :lesson_categories do |t|
      t.string      :name
      t.text        :description
      t.belongs_to  :lesson
      t.timestamps
    end
  end

  def down
    drop_table :lesson_categories
  end
end
