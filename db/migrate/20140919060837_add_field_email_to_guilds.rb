class AddFieldEmailToGuilds < ActiveRecord::Migration
  def up
  	add_column :guilds, :email, :string
  end

  def down
  	remove_column :guilds, :email
  end
end
