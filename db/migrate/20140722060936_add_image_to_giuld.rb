class AddImageToGiuld < ActiveRecord::Migration
  def up
    add_attachment :guilds, :image
  end

  def down
    remove_attachment :guilds, :image
  end
end
