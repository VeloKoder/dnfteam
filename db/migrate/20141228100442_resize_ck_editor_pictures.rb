class ResizeCkEditorPictures < ActiveRecord::Migration
  def change
    Ckeditor::Picture.all.each { |p| p.data.reprocess!; p.save! }
  end
end

