class CreateAwardHistories < ActiveRecord::Migration
  def change
    create_table :award_histories do |t|
    	t.belongs_to :award
    	t.belongs_to :user
      t.timestamps
    end
  end
end
