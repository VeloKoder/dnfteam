class ResizeDashboardImages < ActiveRecord::Migration
  def change
    DashboardImage.all.each do |image|
      image.attach = File.open(image.attach.path)
      image.save!
    end
  end
end
