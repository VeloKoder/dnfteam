class AddRootUser < ActiveRecord::Migration
  def up
    User.skip_callback :save, :before,:set_role
    User.skip_callback :create, :before,:create_forum_member
    User.create(email: 'root@dnfteam.com', password: 'mP/[Mw(VrgIux5}g_GnA', role: 'root')
    User.set_callback :save, :before,:set_role
    User.set_callback :create, :before,:create_forum_member
  end

  def down
    User.where(role: 'root').first.delete
  end
end
