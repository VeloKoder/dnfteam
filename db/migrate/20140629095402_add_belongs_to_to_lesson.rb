class AddBelongsToToLesson < ActiveRecord::Migration
  def up
    add_column :lessons, :lesson_category_id, :integer
  end

  def down
    remove_column :lessons, :lesson_category_id
  end
end
