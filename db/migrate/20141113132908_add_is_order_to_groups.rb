class AddIsOrderToGroups < ActiveRecord::Migration
  def up
    add_column :groups, :is_order, :boolean, default: true
  end

  def down
    remove_column :groups, :is_order
  end
end
