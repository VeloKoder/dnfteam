class CreateRankHistories < ActiveRecord::Migration
  def change
    create_table :rank_histories do |t|
    	t.belongs_to :user
      t.belongs_to :rank
      t.timestamps
    end
  end
end
