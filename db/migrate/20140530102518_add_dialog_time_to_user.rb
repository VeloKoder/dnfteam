class AddDialogTimeToUser < ActiveRecord::Migration
  def change
    add_column :users, :dialog_time, :string
  end
end
