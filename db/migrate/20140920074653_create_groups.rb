class CreateGroups < ActiveRecord::Migration
  def up
    create_table :groups do |t|
      t.string :name
      t.boolean :is_admin
      t.boolean :is_articles
      t.boolean :is_orders
      t.boolean :is_lessons
      t.boolean :is_categories
      t.boolean :is_users
      t.boolean :is_admin_view_user
      t.boolean :is_admin_view_lesson
      t.boolean :is_admin_view_lesson_category
      t.boolean :is_admin_view_category
      t.boolean :is_admin_view_link
      t.boolean :is_admin_view_static_page
      t.boolean :is_admin_view_url
      t.boolean :is_admin_edit_user
      t.boolean :is_admin_edit_lesson
      t.boolean :is_admin_edit_lesson_category
      t.boolean :is_admin_edit_category
      t.boolean :is_admin_edit_link
      t.boolean :is_admin_edit_static_page
      t.boolean :is_admin_edit_url
      t.boolean :is_default

      t.timestamps
    end

    create_table :groups_users do |t|
      t.belongs_to :group
      t.belongs_to :user
    end

    create_table :articles_groups do |t|
      t.belongs_to :group
      t.belongs_to :article
    end

    create_table :categories_groups do |t|
      t.belongs_to :group
      t.belongs_to :category
    end

    create_table :groups_guilds do |t|
      t.belongs_to :group
      t.belongs_to :guild
    end

    create_table :groups_static_pages do |t|
      t.belongs_to :group
      t.belongs_to :static_page
    end

    create_table :groups_lessons do |t|
      t.belongs_to :group
      t.belongs_to :lesson
    end

    create_table :groups_lesson_categories do |t|
      t.belongs_to :group
      t.belongs_to :lesson_category
    end
  end

  def down
    drop_table :groups
    drop_table :groups_users
    drop_table :articles_groups
    drop_table :categories_groups
    drop_table :groups_guilds
    drop_table :groups_static_pages
    drop_table :groups_lessons
    drop_table :groups_lesson_categories
  end
end
