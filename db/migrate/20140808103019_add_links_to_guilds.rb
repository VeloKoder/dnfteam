class AddLinksToGuilds < ActiveRecord::Migration
  
  def up
    add_column :guilds, :charter,   :string
    add_column :guilds, :structure, :string
    add_column :guilds, :orders,    :string
  end

  def down
    remove_column :guilds, :charter
    remove_column :guilds, :structure
    remove_column :guilds, :orders
  end
end
