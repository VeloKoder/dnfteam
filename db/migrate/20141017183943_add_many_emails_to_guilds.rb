class AddManyEmailsToGuilds < ActiveRecord::Migration
  def up
    create_table :emails do |t|
      t.string     :value
      t.belongs_to :guild
      t.timestamps
    end

    Guild.all.each { |guild| Email.create(value: guild.email, guild_id: guild.id) }

    remove_column :guilds, :email
  end

  def down
    add_column :guilds, :email, :string

    Guild.all.each { |guild| guild.update(email: guild.emails.first.value) if guild.emails.present? }

    drop_table :emails
  end
end
