class RemoveGuildIdFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :guild_id
  end
end
