class AddFieldsToUser < ActiveRecord::Migration
  def up
  	add_column :users, :hardware, 	  :string
  	add_column :users, :name, 	  	  :string
  	add_column :users, :patronymic,   :string
  	add_column :users, :surname,	  :string
  	add_column :users, :location,	  :string
  	add_column :users, :birthday,     :date
  	add_column :users, :subscription, :text
  	add_attachment :users, :avatar
  end

  def down
  	remove_column :users, :hardware
  	remove_column :users, :name
  	remove_column :users, :patronymic
  	remove_column :users, :surname
  	remove_cloumn :users, :location
  	remove_column :users, :birthday
  	remove_column :users, :subscription
  	remove_attachment :users, :avatar
  end
end
