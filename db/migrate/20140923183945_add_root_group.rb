class AddRootGroup < ActiveRecord::Migration
  def change
    User.skip_callback :save, :before,:set_role
    Group.create(
      name: 'root',
      is_admin: true,
      is_articles: true,
      is_orders: true,
      is_lessons: true,
      is_categories: true,
      is_users: true,
      is_admin_view_user: true,
      is_admin_view_lesson: true,
      is_admin_view_lesson_category: true,
      is_admin_view_category: true,
      is_admin_view_link: true,
      is_admin_view_static_page: true,
      is_admin_view_url: true,
      is_admin_edit_user: true,
      is_admin_edit_lesson: true,
      is_admin_edit_lesson_category: true,
      is_admin_edit_category: true,
      is_admin_edit_link: true,
      is_admin_edit_static_page: true,
      is_admin_edit_url: true,
      is_default: false,
      users: User.where(role: 'root'),
      static_pages: StaticPage.all,
      lessons: Lesson.all,
      guilds: Guild.all,
      categories: Category.all,
      lesson_categories: LessonCategory.all,
      articles: Article.all
    )
    User.set_callback :save, :before,:set_role
  end
end