class AddFieldInOrderToGuild < ActiveRecord::Migration
  def up
    add_column :guilds, :in_order, :boolean, default: true
  end

  def down
    remove_column :guilds, :in_order
  end
end
