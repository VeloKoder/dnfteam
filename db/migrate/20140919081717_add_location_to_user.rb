class AddLocationToUser < ActiveRecord::Migration
  def up
    add_column :users, :location, :string
    add_column :users, :hardware, :string
  end

  def down
    remove_column :users, :location
    remove_column :users, :hardware
  end
end
