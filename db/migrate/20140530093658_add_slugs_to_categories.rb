class AddSlugsToCategories < ActiveRecord::Migration
  def up
    add_column :categories, :slug, :string
    add_index :categories, :slug, unique: true
  end

  def down
    remove_index :categories, :slug
    remove_column :categories, :slug
  end
end
