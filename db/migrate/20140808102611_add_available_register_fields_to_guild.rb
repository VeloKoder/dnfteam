class AddAvailableRegisterFieldsToGuild < ActiveRecord::Migration
  def up
    add_column :guilds, :register_fields, :string
  end

  def wodn
    remove_column :guilds, :register_fields
  end
end
