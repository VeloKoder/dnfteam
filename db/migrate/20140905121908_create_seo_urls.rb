class CreateSeoUrls < ActiveRecord::Migration
  def change
    create_table :seo_urls do |t|
      t.string     :title
      t.string     :description
      t.timestamps
    end
  end
end
