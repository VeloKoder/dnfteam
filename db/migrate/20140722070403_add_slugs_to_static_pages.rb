class AddSlugsToStaticPages < ActiveRecord::Migration
  def up
    add_column :static_pages, :slug, :string
    add_index  :static_pages, :slug, unique: true
  end

  def down
    remove_index  :static_pages, :slug
    remove_column :static_pages, :slug
  end
end
