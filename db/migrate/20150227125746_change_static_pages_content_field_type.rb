class ChangeStaticPagesContentFieldType < ActiveRecord::Migration
  def up
    change_column :static_pages, :content, :longtext
  end

  def down
    change_column :static_pages, :content, :text
  end
end
